import os
from hashlib import sha256
import logging

from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings

from book_library.forms import AuthorForm, BookForm, CategoryForm
from book_library.models import Author, Book, Category

logging.disable(logging.WARNING)


class AuthorTestCaseStaff(TestCase):
    def setUp(self):
        self.client.force_login(User.objects.get_or_create(username='testuser', is_staff=True)[0])
        self.author_data = {
            'first_name': 'Eda',
            'last_name': 'Pohřinský',
            'birth_date': '1970-01-01',
        }

    def test_author_form_create_without_book_should_pass(self):
        author_count = Author.objects.count()
        form = AuthorForm(data=self.author_data)
        self.assertTrue(form.is_valid())
        form.save(commit=True)
        self.assertEqual(author_count + 1, Author.objects.count())

    def test_create_author_should_pass(self):
        author_count = Author.objects.count()
        self.client.post('/authors/create', data=self.author_data)
        self.assertEqual(author_count + 1, Author.objects.count())

    def test_create_author_without_names_should_fail(self):
        author_count = Author.objects.count()
        del self.author_data['first_name']
        del self.author_data['last_name']
        response = self.client.post('/authors/create', data=self.author_data)
        self.assertFormError(response, 'form', 'first_name', 'This field is required.')
        self.assertFormError(response, 'form', 'last_name', 'This field is required.')
        self.assertEqual(author_count, Author.objects.count())

    def test_create_author_with_image_should_pass(self):
        with self.settings(MEDIA_ROOT=os.path.join(settings.BASE_DIR, 'book_library/test_files')):
            self.author_data['author_pic'] = SimpleUploadedFile(
                name='profile_pic.jpg',
                content=open(f'{settings.BASE_DIR}/book_library/test_files/profile_pic.jpg', mode='rb').read(),
                content_type='image/jpeg')
            author = Author(**self.author_data)
            self.client.post('/authors/create', data=model_to_dict(author))

            default_file = open(f'{settings.BASE_DIR}/book_library/test_files/profile_pic.jpg', mode='rb').read()
            test_file = open(
                f'{settings.BASE_DIR}/book_library/test_files/author_pictures/profile_pic.jpg', mode='rb'
                ).read()

            self.assertEqual(sha256(default_file).hexdigest(), sha256(test_file).hexdigest())

            os.remove(f'{settings.BASE_DIR}/book_library/test_files/author_pictures/profile_pic.jpg')

    def test_author_form_with_image_should_pass(self):
        with self.settings(MEDIA_ROOT=os.path.join(settings.BASE_DIR, 'book_library/test_files')):
            data = {
                **self.author_data,
            }
            files = {
                'author_pic': SimpleUploadedFile(
                    name='profile_pic.jpg',
                    content=open(f'{settings.BASE_DIR}/book_library/test_files/profile_pic.jpg', mode='rb').read(),
                    content_type='image/jpeg')
            }
            form = AuthorForm(data=data, files=files)
            self.assertTrue(form.is_valid())
            form.save(commit=True)

            default_file = open(f'{settings.BASE_DIR}/book_library/test_files/profile_pic.jpg', mode='rb').read()
            test_file = open(
                f'{settings.BASE_DIR}/book_library/test_files/author_pictures/profile_pic.jpg', mode='rb'
                ).read()

            self.assertEqual(sha256(default_file).hexdigest(), sha256(test_file).hexdigest())

            os.remove(f'{settings.BASE_DIR}/book_library/test_files/author_pictures/profile_pic.jpg')


class BookTestCaseStaff(TestCase):
    def setUp(self):
        self.client.force_login(User.objects.get_or_create(username='testuser', is_staff=True)[0])
        self.category = Category.objects.create(title='Crimi')
        self.author = Author.objects.create(first_name='John', last_name='Brown', birth_date=timezone.now())
        self.book_data = {
            'category': self.category,
            'title': 'test',
            'written_at': timezone.now(),
            'description': 'it is a very interesting read',
        }

    def test_book_form_create_should_pass(self):
        self.book_data['category'] = self.category.pk
        book_count = Book.objects.count()
        data = {
            **self.book_data,
            'authors': Author.objects.filter(id=self.author.id),
        }
        form = BookForm(data=data)
        self.assertTrue(form.is_valid())
        form.save(commit=True)
        self.assertEqual(book_count + 1, Book.objects.count())

    def test_create_book_should_pass(self):
        book_count = Book.objects.count()
        data = {
            'category': self.category.pk,
            'written_at': '2010-01-01',
            'title': 'ahojky',
            'description': 'ahoj',
            'authors': Author.objects.filter(id=self.author.id).values_list('pk', flat=True),
        }
        self.client.post('/books/create', data=data)
        self.assertEqual(book_count + 1, Book.objects.count())

    def test_update_book_should_pass(self):
        book = Book.objects.create(**self.book_data)
        data = {
            'category': self.category.pk,
            'written_at': '2010-01-01',
            'title': 'TestTitle',
            'description': 'TestDesc',
            'authors': self.author.pk
        }
        self.client.post(f'/books/update/{book.pk}', data=data)
        book.refresh_from_db()
        self.assertEqual(book.title, data['title'])
        self.assertEqual(book.description, data['description'])

    def test_book_update_form_should_pass(self):
        book = Book.objects.create(**self.book_data)
        book.title = 'TestTitle'
        form = BookForm(data={**model_to_dict(book), 'authors': [self.author.pk]}, instance=book)
        self.assertTrue(form.is_valid())
        print(form.errors)
        updated_book = form.save(commit=True)
        book.refresh_from_db()
        self.assertEqual(updated_book.title, book.title)
        self.assertEqual(updated_book.description, book.description)

    def test_create_book_without_title_should_fail(self):
        book_count = Book.objects.count()
        del self.book_data['title']
        response = self.client.post('/books/create', data=self.book_data)
        self.assertFormError(response, 'form', 'title', 'This field is required.')
        self.assertEqual(book_count, Book.objects.count())

    def test_create_book_with_wrong_written_at_shoud_fail(self):
        book_count = Book.objects.count()
        self.book_data['written_at'] = 'not a date'
        response = self.client.post('/books/create', data=self.book_data)
        self.assertFormError(response, 'form', 'written_at', 'Enter a valid date/time.')
        self.assertEqual(book_count, Book.objects.count())

    def test_book_post_delete_should_pass(self):
        book_count = Book.objects.count()
        book = Book.objects.create(**self.book_data)
        self.client.post(reverse('library:book_delete', args=[book.id]))
        self.assertEqual(book_count, Book.objects.count())

    def test_book_get_delete_should_show_confirm(self):
        book = Book.objects.create(**self.book_data)
        response = self.client.get(reverse('library:book_delete', args=[book.id]))
        self.assertContains(response, 'Opravdu chcete smazat')

    def test_book_get_delete_shouldnt_delete(self):
        book = Book.objects.create(**self.book_data)
        book_count = Book.objects.count()
        self.client.get(reverse('library:book_delete', args=[book.id]))
        self.assertEqual(book_count, Book.objects.count())


class CategoryTestCaseStaff(TestCase):
    def setUp(self):
        self.client.force_login(User.objects.get_or_create(username='testuser', is_staff=True)[0])
        self.category_data = {
            'title': 'testovací kategorie',
        }

    def test_category_form_should_pass(self):
        category_count = Category.objects.count()
        form = CategoryForm(data=self.category_data)
        self.assertTrue(form.is_valid())
        form.save(commit=True)
        self.assertEqual(category_count + 1, Category.objects.count())

    def test_create_category_should_pass(self):
        category_count = Category.objects.count()
        self.client.post('/categories/create', data=self.category_data)
        self.assertEqual(category_count + 1, Category.objects.count())

    def test_create_category_without_title_should_fail(self):
        category_count = Category.objects.count()
        del self.category_data['title']
        response = self.client.post('/categories/create', data=self.category_data)
        self.assertFormError(response, 'form', 'title', 'This field is required.')
        self.assertEqual(category_count, Category.objects.count())


class AuthorTestCaseNotStaff(TestCase):
    def setUp(self):
            self.client.force_login(User.objects.get_or_create(username='testuser', is_staff=False)[0])
            self.author_data = {
                'first_name': 'Eda',
                'last_name': 'Pohřinský',
                'birth_date': '1970-01-01',
            }

    def test_create_author_shouldnt_pass(self):
        author_count = Author.objects.count()
        self.client.post('/authors/create', data=self.author_data)
        self.assertEqual(author_count, Author.objects.count())

    def test_create_author_without_names_should_fail(self):
        author_count = Author.objects.count()
        del self.author_data['first_name']
        del self.author_data['last_name']
        response = self.client.post('/authors/create', data=self.author_data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(author_count, Author.objects.count())


class BookTestCaseNotStaff(TestCase):
    def setUp(self):
        self.client.force_login(User.objects.get_or_create(username='testuser', is_staff=False)[0])
        self.category = Category.objects.create(title='Crimi')
        self.author = Author.objects.create(first_name='John', last_name='Brown', birth_date=timezone.now())
        self.book_data = {
            'category': self.category,
            'title': 'test',
            'written_at': timezone.now(),
            'description': 'it is a very interesting read',
        }

    def test_create_book_shouldnt_pass(self):
        book_count = Book.objects.count()
        response = self.client.post('/books/create', data=self.book_data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(book_count, Book.objects.count())

    def test_create_book_without_title_should_fail(self):
        book_count = Book.objects.count()
        del self.book_data['title']
        response = self.client.post('/books/create', data=self.book_data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(book_count, Book.objects.count())

    def test_create_book_with_wrong_written_at_shoud_fail(self):
        book_count = Book.objects.count()
        self.book_data['written_at'] = 'not a date'
        response = self.client.post('/books/create', data=self.book_data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(book_count, Book.objects.count())

    def test_book_post_delete_should_pass(self):
        book_count = Book.objects.count()
        book = Book.objects.create(**self.book_data)
        response = self.client.post(reverse('library:book_delete', args=[book.id]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(book_count + 1, Book.objects.count())

    def test_book_get_delete_should_show_forbidden(self):
        book = Book.objects.create(**self.book_data)
        response = self.client.get(reverse('library:book_delete', args=[book.id]))
        self.assertEqual(response.status_code, 403)

    def test_book_get_delete_shouldnt_delete(self):
        book = Book.objects.create(**self.book_data)
        book_count = Book.objects.count()
        response = self.client.get(reverse('library:book_delete', args=[book.id]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(book_count, Book.objects.count())


class CategoryTestCaseNotStaff(TestCase):
    def setUp(self):
        self.client.force_login(User.objects.get_or_create(username='testuser', is_staff=False)[0])
        self.category_data = {
            'title': 'testovací kategorie',
        }

    def test_create_category_should_pass(self):
        category_count = Category.objects.count()
        response = self.client.post('/categories/create', data=self.category_data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(category_count, Category.objects.count())

    def test_create_category_without_title_should_fail(self):
        category_count = Category.objects.count()
        del self.category_data['title']
        response = self.client.post('/categories/create', data=self.category_data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(category_count, Category.objects.count())
