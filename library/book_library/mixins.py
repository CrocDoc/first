from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin


class LoginRequiredNoNextMixin(LoginRequiredMixin):
    login_url = '/login'
    redirect_field_name = ''


class StaffUserOnlyMixin(UserPassesTestMixin):
    permission_denied_message = 'You have insufficient rights for this operation.'

    def test_func(self):
        return self.request.user.is_staff
