import logging
import os
from django.conf import settings


class CustomFileHandler(logging.FileHandler):
    def __init__(self, filename, mode='a+', *args, **kwargs):
        if not os.path.exists(os.path.join(settings.BASE_DIR, 'logs')):
            os.makedirs(os.path.join(settings.BASE_DIR, 'logs'))
        super().__init__(filename, mode, *args, **kwargs)
