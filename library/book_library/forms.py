from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .models import Book, Author, Category


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']


class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username']


class BookForm(forms.ModelForm):
    authors = forms.ModelMultipleChoiceField(queryset=Author.objects.all())

    class Meta:
        model = Book
        fields = ['category', 'title', 'written_at', 'description', 'authors']

    def save(self, *args, **kwargs):
        data = self.cleaned_data
        book = super().save(commit=True)
        for author in data['authors']:
            book.author_set.add(author)
        return book


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['author_pic', 'book', 'first_name', 'last_name', 'birth_date']


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['title']
