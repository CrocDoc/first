from django.urls import path
from . import views

app_name = 'library'
urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    # Login urls
    path('login/', views.UserLogin.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('register/', views.UserRegister.as_view(), name='register'),
    # Book urls
    path('books/', views.BookList.as_view(), name='book_list'),
    path('books/<int:pk>', views.BookDetail.as_view(), name="book_detail"),
    path('books/create', views.BookCreate.as_view(), name='book_create'),
    path('books/update/<int:pk>', views.BookFormUpdate.as_view(), name='book_update'),
    path('books/delete/<int:pk>', views.BookDelete.as_view(), name='book_delete'),
    # Author urls
    path('authors/', views.AuthorList.as_view(), name="author_list"),
    path('authors/<int:pk>', views.AuthorDetail.as_view(), name="author_detail"),
    path('authors/create', views.AuthorCreate.as_view(), name='author_create'),
    path('authors/update/<int:pk>', views.AuthorUpdate.as_view(), name='author_update'),
    path('authors/delete/<int:pk>', views.AuthorDelete.as_view(), name='author_delete'),
    # Category urls
    path('categories/', views.CategoryList.as_view(), name="category_list"),
    path('categories/<int:pk>', views.CategoryDetail.as_view(), name="category_detail"),
    path('categories/create', views.CategoryCreate.as_view(), name='category_create'),
    path('categories/update/<int:pk>', views.CategoryUpdate.as_view(), name='category_update'),
    path('categories/delete/<int:pk>', views.CategoryDelete.as_view(), name='category_delete'),
    # Statistic urls
    path('statistics/', views.DateStatistic.as_view(), name='date_statistic')
    ]
