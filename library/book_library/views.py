from .models import Book, Category, Author
from django.views import generic
from django.urls import reverse_lazy
from book_library import forms
from django.contrib.auth import logout
from django.contrib.auth.views import LoginView
from .mixins import LoginRequiredNoNextMixin, StaffUserOnlyMixin
from django.db.models import Min, Max, Avg, Func, F, FloatField, DateField


# Create your views here.
class IndexView(LoginRequiredNoNextMixin, generic.base.TemplateView):
    template_name = 'library/index.html'


# Auth views
class LogoutView(generic.RedirectView):
    url = reverse_lazy('library:login')

    def get_redirect_url(self, *args, **kwargs):
        logout(self.request)
        return super().get_redirect_url(*args, **kwargs)


class UserLogin(LoginView):
    redirect_field_name = ''
    redirect_authenticated_user = True
    authentication_form = forms.LoginForm


class UserRegister(generic.CreateView):
    form_class = forms.RegistrationForm
    template_name = 'registration/register.html'
    success_url = '/login'


# Book Views
class BookDetail(LoginRequiredNoNextMixin, generic.DetailView):
    model = Book
    template_name = 'library/book_detail.html'


class BookList(LoginRequiredNoNextMixin, generic.ListView):
    model = Book
    template_name = 'library/books.html'
    paginate_by = 10
    queryset = Book.objects.all().order_by('pk')


class BookCreate(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.CreateView):
    model = Book
    form_class = forms.BookForm
    template_name = 'library/model_edit.html'


class BookFormUpdate(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.UpdateView):
    model = Book
    form_class = forms.BookForm
    template_name = 'library/model_edit.html'
    success_url = reverse_lazy('library:book_list')


class BookDelete(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.DeleteView):
    model = Book
    template_name = 'library/book_confirm_delete.html'
    success_url = reverse_lazy('library:book_list')


# Author Views
class AuthorDetail(LoginRequiredNoNextMixin, generic.DetailView):
    model = Author
    template_name = 'library/author_detail.html'


class AuthorList(LoginRequiredNoNextMixin, generic.ListView):
    model = Author
    template_name = 'library/authors.html'
    paginate_by = 10
    queryset = Author.objects.all().order_by('pk')


class AuthorCreate(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.CreateView):
    model = Author
    form_class = forms.AuthorForm
    template_name = 'library/model_edit.html'


class AuthorUpdate(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.UpdateView):
    model = Author
    form_class = forms.AuthorForm
    template_name = 'library/model_edit.html'


class AuthorDelete(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.DeleteView):
    model = Author
    template_name = 'library/author_confirm_delete.html'
    success_url = reverse_lazy('library:author_list')


# Category Views
class CategoryDetail(LoginRequiredNoNextMixin, generic.DetailView):
    model = Category
    template_name = 'library/category_detail.html'


class CategoryList(LoginRequiredNoNextMixin, generic.ListView):
    model = Category
    template_name = 'library/categories.html'
    paginate_by = 10
    queryset = Category.objects.all().order_by('pk')


class CategoryCreate(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.CreateView):
    model = Category
    form_class = forms.CategoryForm
    template_name = 'library/model_edit.html'


class CategoryUpdate(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.UpdateView):
    model = Category
    form_class = forms.CategoryForm
    template_name = 'library/model_edit.html'


class CategoryDelete(LoginRequiredNoNextMixin, StaffUserOnlyMixin, generic.DeleteView):
    model = Category
    template_name = 'library/category_confirm_delete.html'
    success_url = reverse_lazy('library:category_list')


# Statistic Views
class DateStatistic(LoginRequiredNoNextMixin, generic.TemplateView):
    template_name = 'library/date_statistics.html'
    context_object_name = 'dates'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['authors_dates'] = Author.objects.aggregate(
            min_date=Min('birth_date'),
            max_date=Max('birth_date'),
            avg_date=Func(
                Avg(
                    Func(
                        F('birth_date'), function='JULIANDAY', output_field=FloatField()
                        )
                    ), function='DATE', output_field=DateField()))
        context['books_dates'] = Book.objects.aggregate(
            min_date=Min('written_at'),
            max_date=Max('written_at'))
        return context
