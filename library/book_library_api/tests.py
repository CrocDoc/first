from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from book_library.models import Author, Book, Category
from django.utils import timezone
from django.forms.models import model_to_dict
from rest_framework import status


class BookAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpassword', is_staff=True)
        self.client.force_authenticate(user=self.user)
        self.category = Category.objects.create(title='TestCat')
        self.book_data = {
            'category': self.category,
            'title': 'test',
            'written_at': timezone.now(),
            'description': 'it is a very interesting read',
        }
        self.model_field_names = ['category', 'title', 'written_at', 'description']
        self.test_book = Book.objects.create(**self.book_data)

    def test_list_books_api_should_pass(self):
        book_count = Book.objects.all().count()
        book_list = self.client.get('/api/books/')
        self.assertEqual(status.HTTP_200_OK, book_list.status_code)

        for book in book_list.data:
            for key in self.model_field_names:
                self.assertTrue(key in book.keys())

        self.assertEqual(book_count, len(book_list.data))

    def test_api_book_create_should_pass(self):
        book_count = Book.objects.all().count()
        response = self.client.post('/api/books/', data=model_to_dict(self.test_book))
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        for key in self.model_field_names:
            self.assertTrue(key in response.data.keys())
        updated_book_count = Book.objects.all().count()
        self.assertEqual(book_count + 1, updated_book_count)

    def test_api_book_unauthorized_request_should_fail(self):
        self.client.logout()
        response = self.client.get('/api/books/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class AuthorAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpassword', is_staff=True)
        self.client.login(username='testuser', password='testpassword')
        self.author_data = {
            'first_name': 'Eda',
            'last_name': 'Pohřinský',
            'birth_date': '1970-01-01',
        }
        self.model_field_names = ['id', 'first_name', 'last_name', 'birth_date', 'book', 'created_at']

    def test_list_authors_api_should_pass(self):
        author_count = Author.objects.all().count()
        author_list = self.client.get('/api/authors/')
        self.assertEqual(status.HTTP_200_OK, author_list.status_code)

        for author in author_list.data:
            for key in self.model_field_names:
                self.assertTrue(key in author.keys())

        self.assertEqual(author_count, len(author_list.data))

    def test_api_author_create_should_pass(self):
        author_count = Author.objects.all().count()
        response = self.client.post('/api/authors/', data=self.author_data)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        for key in self.model_field_names:
            self.assertTrue(key in response.data.keys())

        updated_author_count = Author.objects.all().count()
        self.assertEqual(author_count + 1, updated_author_count)

    def test_api_author_unauthorized_request_should_fail(self):
        self.client.logout()
        response = self.client.get('/api/authors/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class CategoryAPITestCase(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpassword', is_staff=True)
        self.client.login(username='testuser', password='testpassword')
        self.category_data = {
            'title': 'testovací kategorie',
        }
        self.model_field_names = ['id', 'title']
        self.test_category = Category.objects.create(**self.category_data)

    def test_list_categories_api_should_pass(self):
        category_count = Category.objects.all().count()
        category_list = self.client.get('/api/categories/')
        self.assertEqual(status.HTTP_200_OK, category_list.status_code)

        for category in category_list.data:
            for key in self.model_field_names:
                self.assertTrue(key in category.keys())

        self.assertEqual(category_count, len(category_list.data))

    def test_api_category_create_should_pass(self):
        category_count = Category.objects.all().count()
        response = self.client.post('/api/categories/', data=self.category_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

        for key in self.model_field_names:
            self.assertTrue(key in response.data.keys())

        updated_category_count = Category.objects.all().count()
        self.assertEqual(category_count + 1, updated_category_count)

    def test_api_category_unauthorized_request_should_fail(self):
        self.client.logout()
        response = self.client.get('/api/categories/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
