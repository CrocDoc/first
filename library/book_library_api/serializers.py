from book_library.models import Author, Book, Category
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'title')


class ResponseBookSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Book
        fields = ('id', 'category', 'title', 'description', 'written_at')


class RequestBookSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = ('id', 'category', 'title', 'description', 'written_at')


class RequestAuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'first_name', 'last_name', 'birth_date', 'created_at', 'book')


class ResponseAuthorSerializer(serializers.ModelSerializer):
    book = RequestBookSerializer(many=True)

    class Meta:
        model = Author
        fields = ('id', 'first_name', 'last_name', 'birth_date', 'created_at', 'book')
