from book_library.models import Author, Book, Category
from book_library_api.serializers import (
    RequestAuthorSerializer, ResponseAuthorSerializer,
    RequestBookSerializer, ResponseBookSerializer,
    CategorySerializer)
from rest_framework import generics
from rest_framework.response import Response
from book_library_api.permissions import IsAdminOrReadOnly


class BookView(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    permission_classes = (IsAdminOrReadOnly,)
    request_serializer = RequestBookSerializer
    response_serializer = ResponseBookSerializer

    def get_serializer_class(self):
        return self.response_serializer

    def create(self, request, *args, **kwargs):
        request_serializer = self.request_serializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        book = request_serializer.save()
        response_serializer = self.response_serializer(book)
        return Response(response_serializer.data)


class CategoryView(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    permission_classes = (IsAdminOrReadOnly,)
    serializer_class = CategorySerializer


class AuthorView(generics.ListCreateAPIView):
    queryset = Author.objects.all()
    permission_classes = (IsAdminOrReadOnly,)
    request_serializer = RequestAuthorSerializer
    response_serializer = ResponseAuthorSerializer

    def get_serializer_class(self):
        return self.request_serializer

    def create(self, request, *args, **kwargs):
        request_serializer = self.request_serializer(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        author = request_serializer.save()
        response_serializer = self.response_serializer(author)
        return Response(response_serializer.data)
