from django.urls import path
from rest_framework.authtoken import views as authviews
from . import views


app_name = 'library_api'
urlpatterns = [
    path('login/', authviews.obtain_auth_token),
    path('books/', views.BookView.as_view()),
    path('authors/', views.AuthorView.as_view()),
    path('categories/', views.CategoryView.as_view())
]
