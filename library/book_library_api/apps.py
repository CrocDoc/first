from django.apps import AppConfig


class BookLibraryApiConfig(AppConfig):
    name = 'book_library_api'
